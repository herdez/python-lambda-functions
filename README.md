## Lambda Functions

##### In order to solve the next exercises, you have to check docs about `lambda functions`.

1) Define a function that returns a lambda expression. Create a function called `modify_list`. The function takes two arguments, a `list` and a `lambda expression`. The function iterates through the list and applies the lambda expression to every element in the list. Restriction: Use `list comprehension`.

```python
"""Lambda function goes here"""


print(modify_list(numbers, lmbda) == [4, 8, 12, 16])
```

2) Define a lambda expression that converts Celsius to Kelvin. Recall that `0°C + 273.15 = 273.15K`.

```python
"""Lambda expression goes here"""

print(convert(27) == 300.15)
```

3) Convert the list of temperatures below from Celsius to Kelvin.

```python
temps = [16, 20, 30, -30, 25]


# >>> [289.15, 293.15, 303.15, 243.15, 298.15]
``` 

4) Define a function that returns a `lambda expression`.

> Constrainst:

- Write a lambda expression that takes two numbers and returns 1 if one is divisible by the other and zero otherwise. Call the lambda expression `mod`.

- Create `divisor` function that returns `mod`. The function only takes one argument, the first number in the `mod` lambda function. The lambda function `mod` took two arguments.

- The function will check whether a number is divisible by 2. Assign this function to divisible2.

```python
# lambda expression


# divisor function


# divisible2 function



# driver code

print(divisible2(16) == 1)
print(divisible2(3) == 0)

```
